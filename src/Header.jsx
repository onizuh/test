import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { FormControl, Form, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Header({ search, cart }) {
  return (
    <header>
      <Navbar bg="light" expand="lg">
        <Link to="/">
          <Navbar.Brand href="">React-Bootstrap</Navbar.Brand>
        </Link>
        <Form inline onSubmit={(event) => event.preventDefault()}>
          <FormControl
            onChange={search}
            type="text"
            placeholder="Search"
            className="mr-sm-2"
          />
          <Link
            to={{
              pathname: "/checkout",
              state: cart,
            }}
          >
            panier
          </Link>
        </Form>
      </Navbar>
    </header>
  );
}
