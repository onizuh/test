import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Row, Table } from "react-bootstrap";
import { useLocation } from "react-router-dom";
import axios from "axios";
import Header from "./Header";

export default function Checktou() {
  const [offers, setOffers] = useState(null);
  let location = useLocation();
  let reduced =
    location.state.length > 0
      ? location.state.reduce((a, b) => ({ price: a.price + b.price }))
      : { price: 0 };

  useEffect(() => {
    // fetch offers
    let query = "http://henri-potier.xebia.fr/books/";
    location.state.forEach((book) => {
      query += `${book.isbn},`;
    });
    query += "/commercialOffers";
    axios
      .get(query)
      .then(({ data }) => {
        setOffers(data.offers[1]);
      })
      .catch((error) => console.error(error));

    // calculate best offer
  }, [location.state]);

  return (
    <div className="App">
      {/* HEADER */}
      <Header />
      <Container fluid="md">
        {/* LIST */}
        <Row>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Title</th>
                <th>Prix</th>
              </tr>
            </thead>
            <tbody>
              {location.state.map((book, index) => (
                <tr key={book.isbn}>
                  <td>{index}</td>
                  <td>{book.title}</td>
                  <td>{book.price}</td>
                </tr>
              ))}
              <tr>
                <td>Prix</td>
                <td></td>
                <td>{reduced.price}</td>
              </tr>
              <tr>
                <td>Meilleur reduction</td>
                <td>{offers && offers.type}</td>
                <td>{offers && offers.value}</td>
              </tr>
              <tr>
                <td>Total</td>
                <td></td>
                <td>{offers && reduced.price - offers.value}</td>
              </tr>
            </tbody>
          </Table>
        </Row>
      </Container>
    </div>
  );
}
