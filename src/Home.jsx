import React, { useState, useCallback, useEffect } from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Row, Button, Table } from "react-bootstrap";
import Header from "./Header";

export default function Home() {
  const [bookList, setBookList] = useState([]);
  const [cart, setCart] = useState([]);
  const [search, setSearch] = useState("");

  const handleSearch = (event) => {
    setSearch(event.target.value);
  };

  const handleCart = (item) => {
    if (cart.includes(item)) return;
    setCart([...cart, item]);
  };

  const getUser = useCallback(async () => {
    try {
      const response = await axios.get("http://henri-potier.xebia.fr/books");
      setBookList(response.data);
    } catch (error) {}
  }, []);

  useEffect(() => {
    getUser();
  }, [getUser]);

  return (
    <div className="App">
      {/* HEADER */}
      <Header search={handleSearch} cart={cart} />
      <Container fluid="md">
        {/* LIST */}
        <Row>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Title</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {bookList
                .filter((book) => book.title.toLowerCase().includes(search))
                .map((book, index) => (
                  <tr key={book.isbn}>
                    <td>{index}</td>
                    <td>{book.title}</td>
                    <td>
                      <Button onClick={() => handleCart(book)}>+</Button>
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
        </Row>
      </Container>
    </div>
  );
}
